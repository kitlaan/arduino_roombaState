#ifndef DRAW_H
#define DRAW_H

#include <Arduino.h>
#include <brzo_i2c.h>
#include <SSD1306Brzo.h>

#include "roomba.h"

// SDA: D2 aka GPIO4
#define SDA_PIN 4

// SCL: D1 aka GPIO5
#define SCL_PIN 5

extern SSD1306Brzo display;

void drawInit();

void drawSensor(struct sensors &s);

void drawSpinny();

void drawConnectivity();

#endif // DRAW_H
