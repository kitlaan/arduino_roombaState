#ifndef ROOMBA_H
#define ROOMBA_H

#include <Arduino.h>
#include <SoftwareSerial.h>

// DD: D5 aka GPIO14 (black wire)
#define SCI_DD_PIN 14

// RX: D6 aka GPIO12 (green wire)
#define SCI_RX_PIN 12

// TX: D7 aka GPIO13 (red wire)
#define SCI_TX_PIN 13

extern SoftwareSerial rsci;

struct sensors
{
    bool wheelL  : 1;
    bool wheelC  : 1;
    bool wheelR  : 1;

    bool bumpL   : 1;
    bool bumpR   : 1;

    bool cliffL  : 1;
    bool cliffFL : 1;
    bool cliffFR : 1;
    bool cliffR  : 1;

    bool motorDL : 1;
    bool motorDR : 1;
    bool motorV  : 1;
    bool motorMB : 1;
    bool motorSB : 1;

    bool wall    : 1;
    bool vwall   : 1;

    byte dirtL;
    byte dirtR;
};

void sciSetupSerial();

void sciWakeup();

void sciStart();
void sciControl();

void sciModeFullToSafe();
void sciModeSafeToFull();
void sciModeSleep();

void sciSetSong(byte songNum, byte numNotes, const byte* notes, const byte* dur);
void sciPlaySong(byte songNum);

#define SCI_SENSOR_LEN 10
bool sciSensors(struct sensors &data);

#endif // ROOMBA_H
