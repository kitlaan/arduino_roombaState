#include "draw.h"
#include "font.h"

#include <ESP8266WiFi.h>

#define F2C(x,c) ((x) ? (c) : "")

void drawInit()
{
    display.init();
    //display.setFont(ArialMT_Plain_10);
    display.setFont(Roboto_Mono_Light_10);
    display.flipScreenVertically();
    display.drawString(20, 40, "connecting...");
    display.display();
}

void drawSensor(struct sensors &s)
{
    char buf[32];

    // no C wheel
    snprintf(buf, sizeof(buf), "Wheel: %1s %1s",
             F2C(s.wheelL, "L"), F2C(s.wheelR, "R"));
    display.drawString(0, 0, buf);

    snprintf(buf, sizeof(buf), "Bump: %1s %1s",
             F2C(s.bumpL, "L"), F2C(s.bumpR, "R"));
    display.drawString(70, 0, buf);

    snprintf(buf, sizeof(buf), "Cliff: %2s %2s %2s %2s",
             F2C(s.cliffL, "L"), F2C(s.cliffFL, "FL"), F2C(s.cliffFR, "FR"), F2C(s.cliffR, "R"));
    display.drawString(0, 13, buf);

    snprintf(buf, sizeof(buf), "Motor: %2s %2s %2s %2s %2s",
             F2C(s.motorDL, "DL"), F2C(s.motorDR, "DR"),
             F2C(s.motorV, "V"),
             F2C(s.motorMB, "MB"), F2C(s.motorSB, "SB"));
    display.drawString(0, 26, buf);

    snprintf(buf, sizeof(buf), "Dirt: L %3d%% R %3d%%",
             s.dirtL, s.dirtR);
    display.drawString(0, 39, buf);

    //snprintf(buf, sizeof(buf), "%5s %5s",
    //         F2C(s.wall, "WALL"), F2C(s.vwall, "VWALL"));
    //display.drawString(0, 42, buf);
}

void drawSpinny()
{
    //64x128
    static uint32_t spinCount = 0;
    display.drawHorizontalLine(120, 63, spinCount++ % 8);
}

void drawConnectivity()
{
    if (WiFi.status() == WL_CONNECTED) {
        display.setPixel(0, 63);
        display.drawString(30, 51, WiFi.localIP().toString());

        // TODO: icon if connected?
        // TODO: write IP address?  WiFi.localIP();
        // TODO: write num clients?
    }
}
