/**
 * Roomba State
 * OLED display of Roomba state.
 * Control of Roomba via WiFi.
 */

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>

#include <ArduinoOTA.h>

#include <FS.h>
#include <Hash.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>

#include "draw.h"
#include "roomba.h"

#include "config.h"

#if !defined(WIFI_SSID) || !defined(WIFI_PASS) || !defined(WIFI_HOSTNAME)
#error "WIFI_SSID, WIFI_PASS, or WIFI_HOSTNAME not defined"
#endif

SSD1306Brzo display(0x3c, SDA_PIN, SCL_PIN);
SoftwareSerial rsci(SCI_RX_PIN, SCI_TX_PIN);

bool bOTAinProgress = false;
long lastDataCheck = 0;

void setupOTA()
{
    ArduinoOTA.setHostname(WIFI_HOSTNAME);

#if defined(OTA_PASSWORD)
    ArduinoOTA.setPassword(OTA_PASSWORD);
#endif

    ArduinoOTA.onStart([]() {
        bOTAinProgress = true;

        display.clear();
        display.drawString(20, 5, "OTA:");
        display.display();
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        display.drawProgressBar(0, 20, 120, 10, (progress / (total / 100)));
        display.display();
    });
    ArduinoOTA.onEnd([]() {
        bOTAinProgress = false;
    });

    ArduinoOTA.begin();
}

void setup()
{
    pinMode(SCI_DD_PIN, OUTPUT);
    digitalWrite(SCI_DD_PIN, HIGH);

    sciSetupSerial();

    brzo_i2c_setup(SDA_PIN, SCL_PIN, 200);

    drawInit();

    WiFi.hostname(WIFI_HOSTNAME);
    WiFi.begin(WIFI_SSID, WIFI_PASS);

    setupOTA();

    sciWakeup();
    sciStart();
}

void loop()
{
    ArduinoOTA.handle();
    if (bOTAinProgress) {
        return;
    }

    long now = millis();
    if (now - lastDataCheck >= 100) {
        lastDataCheck = now;

        display.clear();

        drawSpinny();
        drawConnectivity();

        struct sensors sens;
        if (sciSensors(sens)) {
            drawSensor(sens);
        }

        display.display();
    }
}
