#include "roomba.h"

#define SCI_BAUD 19200
//#define SCI_BAUD 57600

void sciSetupSerial()
{
    rsci.begin(SCI_BAUD);
}

void sciWakeup()
{
    digitalWrite(SCI_DD_PIN, LOW);
    delay(500);
    digitalWrite(SCI_DD_PIN, HIGH);
    delay(2000);

#if defined(SCI_BAUD) && SCI_BAUD == 19200
    for (int i = 0; i < 3; i++) {
        digitalWrite(SCI_DD_PIN, LOW);
        delay(250);
        digitalWrite(SCI_DD_PIN, HIGH);
        delay(250);
    }
#endif
}

static inline void ControlOp(byte op)
{
    rsci.write(op);
    delay(50);
}

void sciStart()
{
    ControlOp(128);
    // should now be in PASSIVE mode
}

void sciControl()
{
    ControlOp(130);
    // should now be in SAFE mode
}

void sciModeFullToSafe()
{
    ControlOp(131);
}

void sciModeSafeToFull()
{
    ControlOp(132);
}

void sciModeSleep()
{
    ControlOp(133);
}

void sciSetSong(byte songNum, byte numNotes, const byte* notes, const byte* dur)
{
    if (songNum > 15 || numNotes == 0 || numNotes > 16) {
        return;
    }

    rsci.write(140);
    rsci.write(songNum);

    for (int i = 0; i < numNotes; i++) {
        rsci.write(notes[i]);
        rsci.write(dur[i]);
    }
}

void sciPlaySong(byte songNum)
{
    if (songNum > 15) {
        return;
    }

    rsci.write(141);
    rsci.write(songNum);
}

bool sciSensors(struct sensors &data)
{
    rsci.write(142);
    rsci.write(1);

    byte sensordata[SCI_SENSOR_LEN];

    int ix = 0;
    unsigned long timeout = millis() + 300;
    while (millis() < timeout) {
        while (rsci.available()) {
            char c = rsci.read();
            sensordata[ix] = c;
            ix++;
            if (ix == sizeof(sensordata)) {
                break;
            }
        }
    }

    bool datagood = false;
    if (ix == sizeof(sensordata)) {
        datagood = true;

        data.wheelC  = sensordata[0] & 0b10000;
        data.wheelL  = sensordata[0] & 0b01000;
        data.wheelR  = sensordata[0] & 0b00100;
        data.bumpL   = sensordata[0] & 0b00010;
        data.bumpR   = sensordata[0] & 0b00001;

        data.wall    = sensordata[1];

        data.cliffL  = sensordata[2];
        data.cliffFL = sensordata[3];
        data.cliffFR = sensordata[4];
        data.cliffR  = sensordata[5];

        data.vwall   = sensordata[6];

        data.motorDL = sensordata[7] & 0b10000;
        data.motorDR = sensordata[7] & 0b01000;
        data.motorMB = sensordata[7] & 0b00100;
        data.motorV  = sensordata[7] & 0b00010;
        data.motorSB = sensordata[7] & 0b00001;

        data.dirtL   = sensordata[8];
        data.dirtR   = sensordata[9];
    }

    return datagood;
}
