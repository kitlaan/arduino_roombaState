# Overview
Arduino code (for ESP8266) to collect state from a Roomba via SCI and display
on an OLED screen as well as accessible via a web server.

# Parts
The last build used these parts:
* WeMos D1 mini
* 128x64 OLED (using I2C)
* DC-DC Buck Converter
* BiDirectional Logic Converter (3.3V - 5V)

# Schematic
```
TODO
```

# TODO
* all of it
